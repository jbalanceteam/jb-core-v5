package org.itx.jbalance;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.l0.Ubiq;



	@Stateless
	@Local(DbManager.class)
	public class DbManagerBean implements DbManager {
		 private Log log = new Log4JLogger(getClass().getName());
	    @Resource(name="JBTestDS")
	    private DataSource dataSource;
	    
	    @PersistenceContext(unitName="JBv4_2")
	    private EntityManager em;

	    @Override
	    public <E extends Ubiq> E find(Class<E> entityClass, Long id) {
	        return em.find(entityClass, id);
	    }

	    @Override
	    public <E extends Ubiq> E save(E entity) {
	        if(entity.getUId() != null) {
	            entity = em.merge(entity);
	        } else {
	            em.persist(entity);
	        }
	        return entity;
	    }

	  
	    @Override
		public EntityManager getEntityManager(){
	    	return em;
	    }

	    @Override
	    public List<String> takeSnapshot() throws SQLException {
	        log.debug("takeSnapshot() [1] Taking snapshot...");
	        Connection connection = null;
	        Statement statement = null;
	        try {
	            connection = dataSource.getConnection();
	            statement = connection.createStatement();
	            ResultSet rs = statement.executeQuery("SCRIPT");
	            List<String> snapshot = new ArrayList<String>();
	            while(rs.next()) {
	                snapshot.add(rs.getString(1));
	            }
	            log.debug("takeSnapshot() [2] Snapshot successful: "+snapshot.size()+" statement(s) returned");
	            return snapshot;
	        } finally {
	            if(statement != null) {
	                try {
	                    statement.close();
	                } catch(Exception t) {}
	            }

	            if(connection != null) {
	                try {
	                    connection.close();
	                } catch(Exception t) {}
	            }
	        }
	    }

	    @Override
	    public void revertToSnapshot(List<String> snapshot) throws SQLException {
	        Connection connection = null;
	        Statement statement = null;
	        try {
	            connection = dataSource.getConnection();
	            statement = connection.createStatement();
	            log.debug("revertToSnapshot() [1] Dropping schema...");
	            statement.execute("DROP SCHEMA JBalance");
	            log.debug("revertToSnapshot() [2] Schema dropped successfully");
	            statement.execute("RUNSCRIPT FROM 'classpath:h2.sql'");
	            log.debug("revertToSnapshot() [3] Schema recreated successfully");
	            if(snapshot != null) {
	                log.debug("revertToSnapshot() [3] Re-building schema; snapshot size: "+ snapshot == null ? 0 : snapshot.size());
	                for(String s: snapshot) {
	                    if(!s.toLowerCase().matches("\\s*create\\s+(schema|user).*") && !s.toLowerCase().matches("\\s*grant.*")) {
	                        log.trace("revertToSnapshot() [4] Executing statement: "+ s);
	                        statement.execute(s);
	                        log.trace("revertToSnapshot() [5] Statement executed successfully");
	                    }
	                }
	            }
	        } catch(Exception t) {
	            log.warn("revertToSnapshot() [6] Error dropping schema", t);
	        } finally {
	            if(statement != null) {
	                try {
	                    statement.close();
	                } catch(Exception t) {}
	                statement = null;
	            }

	            if(connection != null) {
	                try {
	                    connection.close();
	                } catch(Exception t) {}
	                connection = null;
	            }
	        }
	    }

	    private File getDatabaseDirectory() {
	        File userDirectory = new File(System.getProperty("user.dir"));
	        File targetDirectory = new File(userDirectory, "target");
	        File databaseDirectory = new File(targetDirectory, "test-database");
	        return databaseDirectory;
	    }

	    @Override
	    public void backup(String subDirectoryName) throws IOException {
	        File databaseDirectory = getDatabaseDirectory();
	        File backupDirectory = new File(databaseDirectory, subDirectoryName);
	        if(!backupDirectory.exists()) {
	            if(!backupDirectory.mkdirs()) {
	                log.warn("backupDatabase(backupDirectory={0}) [1] Backup failed: unable to create backup directory");
	                return;
	            }
	        }

	        log.info("backup(subDirectoryName="+subDirectoryName+") [1] Backing up database files to "+backupDirectory.getAbsolutePath());
	        for(File file: databaseDirectory.listFiles()) {
	            if(!file.isDirectory()) {
	                File backupFile = new File(backupDirectory, file.getName());
	                log.debug("backup(subDirectoryName="+subDirectoryName+") [2] "+file+" => "+backupFile);
	                Io.copyFile(file, backupFile);
	            }
	        }
	    }

	
}
