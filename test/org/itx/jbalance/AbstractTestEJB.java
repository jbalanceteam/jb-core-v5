package org.itx.jbalance;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.l1.o.ContractorsRemote;
import org.itx.jbalance.l1.o.JuridicalsRemote;
import org.itx.jbalance.l1.o.OpersRemote;
//import org.itx.jbalance.l1.session.SessionprofileRemote;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 * Базовый класс для EJB тестов
 * @author apv
 */
public abstract class AbstractTestEJB {
 
	protected static Context context;
//	private static Server server;
	protected Log log= new Log4JLogger(getClass().getName());
	private DbManager dbManager;
	private List<String> snapshot;
	
//	protected SessionprofileRemote sessionprofile;
	protected OpersRemote opers;
//	protected DocumentsRemote documents;
	protected ContractorsRemote contractors;
	protected JuridicalsRemote juridicals;
	
	@BeforeClass
	public static void beforeClass() throws Exception{
//		new Log4JLogger(AbstractTestEJB.class.getName()).debug("beforeClass()");
//		Properties properties = new Properties();
//		properties.put(Context.SECURITY_PRINCIPAL, "test");
//		properties.put(Context.SECURITY_CREDENTIALS, "test");
//		properties.setProperty("openejb.authentication.realmName", "PropertiesLogin");
//		properties.setProperty("openejb.embedded.remotable", "true");
//		context = new InitialContext(properties); 
		context = new InitialContext(); 
	}
	
	@AfterClass
	public static void afterClass(){
		new Log4JLogger(AbstractTestEJB.class.getName()).debug("before()");
	}
	
	@Before
	public void before() throws Exception {
		log.trace("before()");
		dbManager = (DbManager)context.lookup("DbManagerBean/local");
        snapshot = dbManager.takeSnapshot();
        opers		   = (OpersRemote)context.lookup("Opers/remote");
        contractors		   = (ContractorsRemote)context.lookup("Contractors/remote");
        juridicals = (JuridicalsRemote)context.lookup("Juridicals/remote");
	}
	
	@After
	public void after()throws Exception {
		log.trace("after()");
		dbManager.revertToSnapshot(snapshot);
        snapshot.clear();
       
	}
}
