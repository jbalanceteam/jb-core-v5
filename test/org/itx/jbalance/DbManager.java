package org.itx.jbalance;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;

import org.itx.jbalance.l0.Ubiq;


/**
 *
 * @author apv
 */
public interface DbManager {
    List<String> takeSnapshot() throws SQLException;
    void revertToSnapshot(List<String> snapshot) throws SQLException;
    void backup(String subDirectoryName) throws IOException;
    
    <E extends Ubiq> E find(Class<E> entityClass, Long id);
    <E extends Ubiq> E save(E entity);
    
    EntityManager getEntityManager();
}
