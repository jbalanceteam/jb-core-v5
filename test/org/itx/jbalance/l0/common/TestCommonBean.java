package org.itx.jbalance.l0.common;

import java.util.Date;
import java.util.List;

import org.itx.jbalance.AbstractTestEJB;
import org.itx.jbalance.l0.o.Oper;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class TestCommonBean extends AbstractTestEJB {
	@Test
	@Ignore
	public void testGetHistory(){
		Oper oper = new Oper();
		oper.setBarCode(1l);
		oper.setBirthday(new Date());
		oper.setCheckingHashCode(1l);
		oper.setDescription("descr");
		oper.setContactInfo("info");
		oper.setEMail("eamil@mail.ru");
		oper.setLogin("some_username");
		oper.setPassphrase("password");
		oper.setRealName("Vasya");
		oper.setSurname("Pupkin");
		oper.setPhone("02");
		oper = opers.create(oper);
		
		oper.setPassphrase("password2");
		Oper oper2 = opers.update(oper);
		
		List<Oper> history = opers.getHistory(oper2);
		Assert.assertEquals(2, history.size());
		
		Assert.assertTrue("password".equals(history.get(0).getPassphrase()));
		Assert.assertTrue("password2".equals(history.get(1).getPassphrase()));

		oper2.setPassphrase("password3");
		Oper oper3 = opers.update(oper2);
		
		history = opers.getHistory(oper3);
		Assert.assertEquals(3, history.size());
	}
	
	/**
	 * #358: В CommonBean поправить метод update_ чтобы он не делал insert если запись не менялась
	 */
	@Test
	@Ignore
	public void testUpdateWithoutChanges(){
		Oper oper = new Oper();
		oper.setBarCode(1l);
		oper.setBirthday(new Date());
		oper.setCheckingHashCode(1l);
		oper.setDescription("descr");
		oper.setContactInfo("info");
		oper.setEMail("eamil@mail.ru");
		oper.setLogin("some_username");
		oper.setPassphrase("password");
		oper.setRealName("Vasya");
		oper.setSurname("Pupkin");
		oper.setPhone("02");
		oper = opers.create(oper);
		
		
//		oper.setPassphrase("password");
		Oper oper2 = opers.update(oper);
		
		List<Oper> history = opers.getHistory(oper2);
		Assert.assertEquals(1, history.size());
		

//		Изменили пароль... должна появиться запись истории
		oper2.setPassphrase("password3");
		Oper oper3 = opers.update(oper2);
		
		history = opers.getHistory(oper3);
		Assert.assertEquals(2, history.size());
		

//		Не меняли ничего... Не должно быть прибывления в истории
		Oper oper4 = opers.update(oper3);
		Assert.assertEquals(2, history.size());
	}

}
