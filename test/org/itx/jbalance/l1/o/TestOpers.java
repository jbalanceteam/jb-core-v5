package org.itx.jbalance.l1.o;

import java.util.Date;
import java.util.List;

import org.itx.jbalance.AbstractTestEJB;
import org.itx.jbalance.l0.o.Oper;
import org.junit.Assert;
import org.junit.Test;

public class TestOpers extends AbstractTestEJB {
	@Test
	public void testCreateGet(){
		Oper oper = new Oper();
		oper.setBarCode(1l);
		oper.setBirthday(new Date());
		oper.setCheckingHashCode(1l);
		oper.setDescription("descr");
		oper.setContactInfo("info");
		oper.setEMail("eamil@mail.ru");
		oper.setLogin("some_username");
		oper.setPassphrase("password");
		oper.setRealName("Vasya");
		oper.setSurname("Pupkin");
		oper.setPhone("02");
		Oper object = opers.create(oper);

		List<Oper> byLogin = opers.getByLogin(oper.getLogin());
		
		Assert.assertNotNull(byLogin);
		Assert.assertEquals(byLogin.size(), 1);
		Oper oper2 = byLogin.get(0);
		Assert.assertEquals(oper.getDescription(), oper2.getDescription());
		Assert.assertEquals(oper.getBirthday(), oper2.getBirthday());
		Assert.assertEquals(oper.getContactInfo(), oper2.getContactInfo());
		Assert.assertEquals(oper.getEMail(), oper2.getEMail());
		Assert.assertEquals(oper.getLogin(), oper2.getLogin());
		Assert.assertEquals(oper.getPhone(), oper2.getPhone());
		Assert.assertEquals(oper.getSurname(), oper2.getSurname());

	}
	
	
	@Test
	public void testGetCount(){
		opers.getCount(Oper.class);
	}
	
	@Test
	public void testGetByUid(){
		Oper oper = new Oper();
		oper.setBarCode(1l);
		oper.setBirthday(new Date());
		oper.setCheckingHashCode(1l);
		oper.setDescription("descr");
		oper.setContactInfo("info");
		oper.setEMail("eamil@mail.ru");
		oper.setLogin("some_username");
		oper.setPassphrase("password");
		oper.setRealName("Vasya");
		oper.setSurname("Pupkin");
		oper.setPhone("02");
		oper = opers.create(oper);
		
		Oper oper2 = opers.getByUId(oper.getUId(), Oper.class);
		Assert.assertEquals(oper2, oper);
	}
	
	@Test
	public void testGetAdminAutocreating(){
		Oper oper = new Oper();
		oper.setBarCode(1l);
		oper.setBirthday(new Date());
		oper.setCheckingHashCode(1l);
		oper.setDescription("descr");
		oper.setContactInfo("info");
		oper.setEMail("eamil@mail.ru");
		oper.setLogin("some_username");
		oper.setPassphrase("password");
		oper.setRealName("Vasya");
		oper.setSurname("Pupkin");
		oper.setPhone("02");
		opers.create(oper);

		
		
//		List<Oper> byLogin = opers.getByLogin("admin");
//		Assert.assertNotNull(byLogin);
//		Assert.assertEquals(1, byLogin.size());
		
		List<?> all = opers.getAll(Oper.class);
		Assert.assertNotNull(all);
		Assert.assertEquals(2, all.size());
		
		Long count = opers.getCount(Oper.class);
		Assert.assertEquals( 2l, count.longValue());
	}

}
