package org.itx.jbalance.l0.o;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class Passport implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
	 * Серия
	 */
	@Column(length=4, name="passport_seria")
	private String seria;
	/**
	 * Номер
	 */
	@Column(length=6, name="passport_number")
	private String number;
	/**
	 * Дата выдачи
	 */
	@Temporal(TemporalType.DATE)
	@Column(name="passport_dateOfIssue")
	private Date dateOfIssue;
	/**
	 * Орган выдавший документ
	 */
	@Column(length=60, name="passport_authority")
	private String authority;
	
	public String getSeria() {
		return seria;
	}
	public void setSeria(String seria) {
		this.seria = seria;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Date getDateOfIssue() {
		return dateOfIssue;
	}
	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
}
