package org.itx.jbalance.l0.o;

import java.io.Serializable;
//import java.util.LinkedHashSet;
//import java.util.Set;

//import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

//import org.itx.jbalance.l0.o.security.Role;


/**
 * пользователь системы
 * @author  dima
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Oper.All",query="from Oper o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Oper.AllCount",query="select count(o) from Oper o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Oper.Search",query="from Oper o where o.version is null and o.closeDate is null and (o.name like :Pref or o.login like :Pref or o.barCode like :Pref) order by o.name"),
	@NamedQuery(name="Oper.SearchCount",query="select count(o) from Oper o where o.version is null and o.closeDate is null and (o.name like :Pref or o.login like :Pref or o.barCode like :Pref) order by o.name"),
	@NamedQuery(name="Oper.ByLogin",query="from Oper o where o.version is null and o.closeDate is null and o.login=:Login order by o.name")
})
public class Oper extends Employee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4276156980772183791L;
		
	
	private String Login;

	private String Passphrase;


//	private Set<Role>roles=new LinkedHashSet<Role>();
	
	
//	@ManyToMany(cascade=CascadeType.ALL, targetEntity=Role.class)
//	public Set<Role> getRoles() {
//		return roles;
//	}

//	public void setRoles(Set<Role> roles) {
//		this.roles = roles;
//	}

	/**
	 * @return  Returns the login.
	 * @uml.property  name="login"
	 */
	public String getLogin() {
		return Login;
	}

	/**
	 * @param login  The login to set.
	 * @uml.property  name="login"
	 */
	public void setLogin(String login) {
		Login = login;
	}

	/**
	 * @return  Returns the passphrase.
	 * @uml.property  name="passphrase"
	 */
//	@Lob
	public String getPassphrase() {
		return Passphrase;
	}

	/**
	 * @param passphrase  The passphrase to set.
	 * @uml.property  name="passphrase"
	 */
	public void setPassphrase(String password) {
		Passphrase = password;
	}
}
