package org.itx.jbalance.l0.o;

import javax.persistence.MappedSuperclass;

import org.hibernate.validator.NotNull;
import org.itx.jbalance.l0.Ubiq;

/**
 * @author  dima
 */
//@Entity
//@NamedQueries({
//	@NamedQuery(name="Relator.All",query="from Relator o where o.version is null and o.closeDate is null order by o.name"),
//	@NamedQuery(name="Relator.AllCount",query="select count(o) from Relator o where o.version is null and o.closeDate is null"),
//	@NamedQuery(name="Relator.Search",query="from Relator o where o.version is null and o.closeDate is null and (o.name like :Pref or o.barCode like :Pref) order by o.name"),
//	@NamedQuery(name="Relator.SearchCount",query="select count(o) from Relator o where o.version is null and o.closeDate is null and (o.name like :Pref or o.barCode like :Pref) order by o.name")
//})
@MappedSuperclass
public abstract class Relator<C extends Relator<?>> extends Ubiq<C>  implements Comparable<C>{

	
	public Relator(){}
	
	public Relator(String name, String description) {
		super();
		Name = name;
		this.description = description;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4171292306687715913L;
	/**
	 * 
	 */

	private String Name;

	
	private String description;

	/**
	 * @return  Returns the name.
	 * @uml.property  name="name"
	 */
	@NotNull
	public String getName() {
		return Name;
	}

	/**
	 * @param name  The name to set.
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		Name = name;
	}
	
	@Override
	public String toString() {
		return Name;
	}
	
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int compareTo(C c){
		if(this==null || this.getName()==null)
			return -1;
		if(c==null || c.getName()==null)
			return 1;
		return this.getName().compareTo(c.getName());
	}

	
}
