//package org.itx.jbalance.l0.o.security;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.ManyToOne;
//import javax.persistence.PrimaryKeyJoinColumn;
//
//import org.itx.jbalance.l0.Ubiq;
//import org.itx.jbalance.l0.o.Oper;
//
//
///**
// * @author  apv
// */
//@Entity
//@PrimaryKeyJoinColumn(name = "UId")
//public class OperRight extends Ubiq {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 4276156980772183791L;
//		
//	/**
//	 * 
//	 */
//	private Oper oper;
//
//	/**
//	 * 
//	 */
//	private SystemAction systemAction;
//
//	@ManyToOne(cascade=CascadeType.ALL,optional=false)
//	public Oper getOper() {
//		return oper;
//	}
//
//	public void setOper(Oper oper) {
//		this.oper = oper;
//	}
//
//	@ManyToOne(cascade=CascadeType.ALL,optional=false)
//	public SystemAction getSystemAction() {
//		return systemAction;
//	}
//
//	public void setSystemAction(SystemAction systemAction) {
//		this.systemAction = systemAction;
//	}
//
//	
//}
