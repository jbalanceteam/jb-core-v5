//package org.itx.jbalance.l0.o.security;
//
//import java.util.Set;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.ManyToMany;
//import javax.persistence.ManyToOne;
//import javax.persistence.PrimaryKeyJoinColumn;
//
//import org.hibernate.validator.NotNull;
//import org.itx.jbalance.l0.Ubiq;
//import org.itx.jbalance.l0.o.Oper;
//
//
///**
// * Действие над объектом системы
// * @author  apv
// */
//@Entity
//@PrimaryKeyJoinColumn(name = "UId")
//public class SystemAction extends Ubiq {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 4276156980772183791L;
//		
//	/**
//	 * Наименование действия.
//	 */
//	private String name;
//	/**
//	 * Описание действия
//	 */
//	private String description;
//	/**
//	 * Объект
//	 */
//	private SystemObject systemObject;
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	public String getDescription() {
//		return description;
//	}
//	public void setDescription(String description) {
//		this.description = description;
//	}
//
//	
//	@NotNull
//	@ManyToOne(cascade=CascadeType.ALL,optional=false,fetch=FetchType.EAGER)
//	public SystemObject getSystemObject() {
//		return systemObject;
//	}
//	public void setSystemObject(SystemObject systemObject) {
//		this.systemObject = systemObject;
//	}
//	
//	
//	
//	
//}
