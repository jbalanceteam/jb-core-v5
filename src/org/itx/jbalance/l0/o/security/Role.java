//package org.itx.jbalance.l0.o.security;
//
//import java.util.Set;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.ManyToMany;
//import javax.persistence.PrimaryKeyJoinColumn;
//
//import org.itx.jbalance.l0.Ubiq;
//import org.itx.jbalance.l0.o.Oper;
//
//
///**
// * пользователь системы
// * @author  apv
// */
//@Entity
//@PrimaryKeyJoinColumn(name = "UId")
//public class Role extends Ubiq {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 4276156980772183791L;
//		
//	/**
//	 * Наименование роли.
//	 * Например: бухгалтер, администратор...
//	 */
//	private String name;
//	/**
//	 * Описание роли
//	 */
//	private String description;
//	/**
//	 * Пользователи, принадлежащие к роли
//	 */
//	private Set<Oper> opers;
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	public String getDescription() {
//		return description;
//	}
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	@ManyToMany(cascade=CascadeType.ALL,mappedBy="roles",targetEntity=Oper.class)
//	public Set<Oper> getOpers() {
//		return opers;
//	}
//	public void setOpers(Set<Oper> opers) {
//		this.opers = opers;
//	}
//	
//	
//}
