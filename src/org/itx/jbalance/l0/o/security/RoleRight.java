//package org.itx.jbalance.l0.o.security;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.ManyToOne;
//import javax.persistence.PrimaryKeyJoinColumn;
//
//import org.itx.jbalance.l0.Ubiq;
//
///**
// * @author  apv
// */
//@Entity
//@PrimaryKeyJoinColumn(name = "UId")
//public class RoleRight extends Ubiq {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 4276156980772183791L;
//		
//	/**
//	 * 
//	 */
//	private Role role;
//
//	/**
//	 * 
//	 */
//	private SystemAction systemAction;
//
//	@ManyToOne(cascade=CascadeType.ALL,optional=false)
//	public Role getRole() {
//		return role;
//	}
//
//	public void setRole(Role role) {
//		this.role = role;
//	}
//
//	@ManyToOne(cascade=CascadeType.ALL,optional=false)
//	public SystemAction getSystemAction() {
//		return systemAction;
//	}
//
//	public void setSystemAction(SystemAction systemAction) {
//		this.systemAction = systemAction;
//	}
//
//	
//}
