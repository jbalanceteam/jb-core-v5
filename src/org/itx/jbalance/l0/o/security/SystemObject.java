//package org.itx.jbalance.l0.o.security;
//
//import javax.persistence.Entity;
//import javax.persistence.PrimaryKeyJoinColumn;
//
//import org.itx.jbalance.l0.Ubiq;
//
//
///**
// * Объект системы
// * @author  apv
// */
//@Entity
//@PrimaryKeyJoinColumn(name = "UId")
//public class SystemObject extends Ubiq {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 4276156980772183791L;
//		
//	/**
//	 * Наименование объекта.
//	 */
//	private String name;
//	/**
//	 * Описание объекта
//	 */
//	private String description;
//
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	public String getDescription() {
//		return description;
//	}
//	public void setDescription(String description) {
//		this.description = description;
//	}
//}
