package org.itx.jbalance.l0.o;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Физическое лицо
 * @author  dima
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Physical.All",query="from Physical o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Physical.AllCount",query="select count(o) from Physical o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Physical.Search",query="from Physical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.surname like :Pref or o.barCode like :Pref) order by o.name"),
	@NamedQuery(name="Physical.SearchCount",query="select count(o) from Physical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.surname like :Pref or o.barCode like :Pref) order by o.name")
})

public class Physical extends Contractor implements Serializable {

	private static final long serialVersionUID = -4426847307209004511L;
	/**
	 * Имя
	 */
	private String RealName;
	/**
	 * Фамилия
	 */
	private String Surname; 
	/**
	 * Отчество
	 */
	private String Patronymic;
	/**
	 * Серия и номер паспорта
	 * TODO физ.лицо может иметь несколько паспортов?
	 */
	private Passport passport = new Passport();
	
	private String fax;  
	/**
	 * Дата рождения
	 */
	@Temporal(TemporalType.DATE)
	private Date birthday;
	
	@Embedded
	public Passport getPassport() {
//		if(passport == null){
//			passport = new Passport();
//		}
		return passport;
	}
	
	public void setPassport(Passport passport) {
		this.passport = passport;
	}
	public String getPatronymic() {
		return Patronymic;
	}
	public void setPatronymic(String patronymic) {
		Patronymic = patronymic;
	}

	public String getRealName() {
		return RealName;
	}

	public void setRealName(String realName) {
		RealName = realName;
	}

	/**
	 * Фамилия
	 * @return
	 */
	public String getSurname() {
		return Surname;
	}

	public void setSurname(String surname) {
		Surname = surname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
}
