package org.itx.jbalance.l0.o;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


/**
 * @author  dima
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Contractor.All",query="from Contractor o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Contractor.AllCount",query="select count(o) from Contractor o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Contractor.Search",query="from Contractor o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.PAddress like :Pref or o.contactInfo like :Pref) order by o.name"),
	@NamedQuery(name="Contractor.SearchCount",query="select count(o) from Contractor o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.PAddress like :Pref or o.contactInfo like :Pref)")
})
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Contractor extends Relator<Contractor>  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2625044317286755343L;
	/**
	 * 
	 */
	private String INN; 
	private String PAddress;
	private String ContactInfo;
	private String Phone;
	private String EMail;
	
	/**
	 * @return  Returns the eMail.
	 * @uml.property  name="eMail"
	 */
	public String getEMail() {
		return EMail;
	}
	/**
	 * @param eMail  The eMail to set.
	 * @uml.property  name="eMail"
	 */
	public void setEMail(String mail) {
		EMail = mail;
	}
	/**
	 * @return  Returns the contactInfo.
	 * @uml.property  name="contactInfo"
	 */
	public String getContactInfo() {
		return ContactInfo;
	}
	/**
	 * @param contactInfo  The contactInfo to set.
	 * @uml.property  name="contactInfo"
	 */
	public void setContactInfo(String contactInfo) {
		ContactInfo = contactInfo;
	}
	
	/**
	 * @return  Returns the phone.
	 * @uml.property  name="phone"
	 */
	public String getPhone() {
		return Phone;
	}
	
	/**
	 * @param phone  The phone to set.
	 * @uml.property  name="phone"
	 */
	public void setPhone(String phone) {
		Phone = phone;
	}
	/**
	 * @return  Returns the iNN.
	 * @uml.property  name="iNN"
	 */
	public String getINN() {
		return INN;
	}
	/**
	 * @param iNN  The iNN to set.
	 * @uml.property  name="iNN"
	 */
	public void setINN(String inn) {
		INN = inn;
	}
	/**
	 * @return  Returns the pAddress.
	 * @uml.property  name="pAddress"
	 */
	public String getPAddress() {
		return PAddress;
	}
	/**
	 * @param pAddress  The pAddress to set.
	 * @uml.property  name="pAddress"
	 */
	public void setPAddress(String address) {
		PAddress = address;
	}
}
