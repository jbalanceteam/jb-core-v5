package org.itx.jbalance.l0.s;


import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.itx.jbalance.l0.h.Hcontractor;
import org.itx.jbalance.l0.o.Contractor;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Scontractor extends Sdocument<Scontractor,Hcontractor> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1913872132276081727L;

	protected Contractor contractor;
	
//	private Scontractor Source;
	
//	public abstract Contractor getArticleUnit();
//	public abstract void setArticleUnit(Contractor contractor);
	
//	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
//	@JoinColumn(name = "Contractor", nullable = true)
//	public Contractor getArticleUnit() {
//		return contractor;
//	}
//
//	public void setArticleUnit(Contractor contractor) {
//		this.contractor = contractor;
//	}

//	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
//	@JoinColumn(name = "Scontractor", nullable = true)
//	public Scontractor getSource() {
//		return Source;
//	}
//
//	public void setSource(Scontractor source) {
//		Source = source;
//	}
}
