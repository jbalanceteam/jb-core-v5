package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.Hdocument;

/**
 * @author  dima
 */
//@Entity
//@NamedQueries({
//	@NamedQuery(name="Document.Specification",query="from Sdocument o where o.version is null and o.closeDate is null and o.HUId = :Header order by o.seqNumber"),
//	@NamedQuery(name="Document.MaxSeqNumber",query="select max(o.seqNumber) from Sdocument o where CloseDate is null and Version is null and HUId = :Header")
//})
//@PrimaryKeyJoinColumn(name = "UId")
@MappedSuperclass
public abstract class Sdocument<C extends Sdocument<?,?>,B extends Hdocument<?>> extends Ubiq<C> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1079135099710366724L;

	private int SeqNumber;

	private B HUId;

	/**
	 * @return  Returns the hUId.
	 * @uml.property  name="hUId"
	 */
	@ManyToOne(optional = false, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "HUId", nullable = false)
	public B getHUId() {
		return HUId;
	}

	/**
	 * @param hUId  The hUId to set.
	 * @uml.property  name="hUId"
	 */
	public void setHUId(B header) {
		HUId = header;
	}

	/**
	 * @return  Returns the seqNumber.
	 * @uml.property  name="seqNumber"
	 */
	public int getSeqNumber() {
		return SeqNumber;
	}

	/**
	 * @param seqNumber  The seqNumber to set.
	 * @uml.property  name="seqNumber"
	 */
	public void setSeqNumber(int seqNumber) {
		SeqNumber = seqNumber;
	}
	
	@Override
	public String toString() {
		return HUId+"/"+SeqNumber;
	}
}