package org.itx.jbalance.l0;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlTransient;

import org.itx.jbalance.l0.o.Oper;

/**
 * @author dima
 */
//@Entity
//@Name("ubiq")
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

//@NamedQueries({
//	@NamedQuery(name="Ubiq.All",query="from Ubiq o where o.version is null and o.closeDate is null order by o.className"),
//	@NamedQuery(name="Ubiq.ByUId",query="from Ubiq o where o.version is null and o.closeDate is null and o.UId = :UId"),
//	@NamedQuery(name="Ubiq.PrevVersion",query="from Ubiq o where o.version = :version"),
//	@NamedQuery(name="Ubiq.Search",query="from Ubiq o where o.version is null and o.closeDate is null and o.barCode like :Pref")
//})

@MappedSuperclass
public abstract class Ubiq<C extends Ubiq<?>> implements Serializable {


	private static final long serialVersionUID = 7191409438438573891L;

	/**
	 * @param UId -
	 *            global uniq identifier
	 */
	private Long UId;

	private Long CheckingHashCode;

	private Timestamp TS;

	private Date OpenDate;

	private Date CloseDate;

//	private String ClassName;

	private Oper SysUser;
	
	private C Version;

	private C VersionRoot;

	private Long BarCode;
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((UId == null) ? 0 : UId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Ubiq))
			return false;
		Ubiq that = (Ubiq) other;
		try {
			return this==that || (this.UId.equals(that.UId) && this.getClass().equals(that.getClass()));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * @return Returns the closeDate.
	 * @uml.property name="closeDate"
	 */
	public Date getCloseDate() {
		return CloseDate;
	}

	/**
	 * @param closeDate
	 *            The closeDate to set.
	 * @uml.property name="closeDate"
	 */
	public void setCloseDate(Date closeDate) {
		CloseDate = closeDate;
	}

	/**
	 * @return Returns the checkingHashCode.
	 * @uml.property name="checkingHashCode"
	 */
	public Long getCheckingHashCode() {
		return CheckingHashCode;
	}

	/**
	 * @param checkingHashCode
	 *            The checkingHashCode to set.
	 * @uml.property name="checkingHashCode"
	 */
	public void setCheckingHashCode(Long chc) {
		CheckingHashCode = chc;
	}

	/**
	 * @return Returns the openDate.
	 * @uml.property name="openDate"
	 */
	public Date getOpenDate() {
		return OpenDate;
	}

	/**
	 * @param openDate
	 *            The openDate to set.
	 * @uml.property name="openDate"
	 */
	public void setOpenDate(Date openDate) {
		OpenDate = openDate;
	}

	@Version
//	@XmlJavaTypeAdapter( TimestampAdapter.class)
	public Timestamp getTS() {
		return TS;
	}

	public void setTS(Timestamp ts) {
		TS = ts;
	}

	/**
	 * @return Returns the uId.
	 * @uml.property name="uId"
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	public Long getUId() {
		return UId;
	}

	public void setUId(Long id) {
		UId = id;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "SysUser", nullable = true)
	@XmlTransient
	public Oper getSysUser() {
		return SysUser;
	}

	public void setSysUser(Oper sysUser) {
		SysUser = sysUser;
	}

	@OneToOne(optional = true,fetch=FetchType.LAZY)
	@JoinColumn(name = "Version", nullable = true)
	@XmlTransient
	public C getVersion() {
		return Version;
	}

	public void setVersion(C version) {
		Version = version;
	}

	@Column(nullable = true)
	public Long getBarCode() {
		return BarCode;
	}

	public void setBarCode(Long barCode) {
		BarCode = barCode;
	}

	@ManyToOne(optional = true, fetch=FetchType.LAZY)
	@JoinColumn(name = "VersionRoot", nullable = true)
	@XmlTransient
	public C getVersionRoot() {
		return VersionRoot;
	}

	public void setVersionRoot(C versionRoot) {
		VersionRoot = versionRoot;
	}
}
