package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Документ для договоров
 * @author apv
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Hcontractor extends Hdocument<Hcontractor> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6131475090397734966L;

	
	/**
	 * Документ прошел по бухгалтерии
	 */
	private Boolean AccChk;

	public Boolean getAccChk() {
		return AccChk;
	}

	public void setAccChk(Boolean accChk) {
		AccChk = accChk;
	}
}
