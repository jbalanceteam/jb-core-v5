package org.itx.jbalance.l0.h;


public interface HierarchicalHeader {
	public Hdocument getHigherDocument();

	public void setHigherDocument(Hdocument higherDocument);
}
