package org.itx.jbalance.l1;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;

import org.itx.jbalance.l0.Ubiq;

public class Utils {

	static public void copy(Object src, Object dst) {
		HashMap<String, Method> dstHM = new HashMap<String, Method>();
		

		Method md[] = dst.getClass().getMethods();
		for (Method m : md) {
			if ((m.getModifiers() == 1) && (m.getName().startsWith("set")))
				dstHM.put(m.getName().replaceFirst("set", "get"), m);
		}
		Method ms[] = src.getClass().getMethods();
		for (Method m : ms) {
			if ((m.getModifiers() == 1) && (m.getName().startsWith("get"))
					&& dstHM.containsKey(m.getName()))
				try {
					Object o[] = { m.invoke(src, (Object[]) null) };
					dstHM.get(m.getName()).invoke(dst, o);
				} catch (Exception e) {
					System.err
							.println("Busines object reflection error in method: "
									+ m.getName());
				}
		}
		dstHM = null;
	}

	
	/**
	 * Метод пробегает по полям объектов и иравнивает их...
	 *  
	 * @return true - если есть различия
	 * 		   false - если объект не менялся
	 */
	static public   boolean isThereChanges(Ubiq<?> o1, Ubiq<?> o2){
		if(! o1.getClass().equals(o2.getClass())){
			throw new RuntimeException();
		}
		
		Method allMethods[] = o1.getClass().getMethods();
		
		
		for (Method method : allMethods) {
			if (method.getModifiers() == Modifier.PUBLIC && 
					(method.getName().startsWith("get") || method.getName().startsWith("is"))){
				try {
					Object res1 = method.invoke(o1);
					Object res2 = method.invoke(o2);
					
					if(res1 == null && res2 == null){
//						return false;
					} else if(res1 != null && res2 == null){
						return true;
					} else if(res1 == null && res2 != null){
						return true;
					} else {
						if(!res1.equals(res2)){
							return true;
						}
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					return true;
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					return true;
				} catch (InvocationTargetException e) {
					e.printStackTrace();
					return true;
				}
				
			}
		}
		return false;
	}

}


