package org.itx.jbalance.l1.session;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l1.o.Opers;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.logging.Logger;

//@Remote(SessionprofileRemote.class)
//@Stateful(name = "Sessionprofile")
//@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 900, removalTimeoutSeconds = 3600)
@LocalBinding(jndiBinding = "Sessionprofile/local")
//@RemoteBinding(jndiBinding = "Sessionprofile/remote")
@Local
@Stateless
public class SessionprofileBean implements Sessionprofile /*,SessionprofileRemote*/ {

	private static final long serialVersionUID = 901001508194964134L;

//	private Config config;

//	@IgnoreDependency
//	@EJB(name = "Opers/local")
//	private Opers opers;

	
	@PersistenceContext(unitName = "JBv4_2")
	protected EntityManager manager;
	
	Logger logger = Logger.getLogger(getClass());
	
	public Date getDate(){return new Date();}
	 
	
	@Resource
	protected SessionContext sessionContext;
	

	/**
	 * Внимание!! Этот метод имеет побочный эффект! 
	 * Он создает пользователя в БД, если он залогинился в сиетму, но в БД его нет
	 * @return
	 */
	private Oper getLoggedUser(){
		try{
			Principal principal =  sessionContext.getCallerPrincipal() ;
			logger.debug("getLoggedUser() principal:'"+principal+"'");
			String userLogin ;

		   
			if(principal!=null){
				userLogin = principal.getName();
				if(userLogin==null || userLogin.isEmpty())
					return null;
				Opers opersEJB = getOpers();
				List<?> opersList = opersEJB.getByLogin(userLogin);
				Oper oper = null;
				if(!opersList.isEmpty())
				  oper = (Oper) opersList.get(0);
				logger.debug("opers.getByLogin("+userLogin+")=> '"+oper+"'");
				
				if(oper == null){
					oper = createOper(userLogin, userLogin);
				}
				
				return oper;
			} else {
			    logger.warn("principal=> '"+principal+"'");
				return null;
			}
			
			
			/*
			 * 
			 * at org.jboss.remoting.transport.socket.ServerThread.dorun(ServerThread.java:406)
	at org.jboss.remoting.transport.socket.ServerThread.run(ServerThread.java:173)
Caused by: java.lang.IllegalStateException: No valid security context for the caller identity
	at org.jboss.ejb3.BaseSessionContext.getCallerPrincipal(BaseSessionContext.java:190)
	at org.itx.jbalance.l1.session.SessionprofileBean.getLoggedUser(SessionprofileBean.java:58)
	at org.itx.jbalance.l1.session.SessionprofileBean.getConfig(SessionprofileBean.java:193)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
	at java.lang.reflect.Method.invoke(Method.java:597)
	at org.jboss.aop.joinpoint.MethodInvocation.invokeNext(MethodInvocation.java:112)
	at org.jboss.ejb3.interceptor.InvocationContextImpl.proceed(InvocationContextImpl.java:166)
	at org.jboss.ejb3.interceptor.EJB3InterceptorsInterceptor.invoke(EJB3InterceptorsInterceptor.java
			 * 
			 */
		} catch(IllegalStateException e){
			logger.warn(e);
			return null;
		}
		
	}
	
	
//	public void setUser() {
//
//		Principal principal =  sctx.getCallerPrincipal() ;//SecurityAssociation.getPrincipal();
//	    String userLogin ;
//
//		if(principal!=null)
//			userLogin = principal.getName();
//		else
////			userLogin = "admin";
//			throw new RuntimeException("SecurityAssociation.getPrincipal() mustn't be null. Check your jaas.config.");
//		
//		logger.debug("AutorisationUserName='"+userLogin+"'     principal.toString() = '"+principal+"'");
//		
//		Oper oper = null;
//
//		
//		try {
//			userLogin="admin";
//			List opersList = opers.getByLogin(userLogin);
//			if(!opersList.isEmpty())
//			  oper = (Oper) opersList.get(0);
//			logger.debug("opers.getByLogin("+userLogin+")=> '"+oper+"'");
//		} catch (Exception e) {
//			logger.info("",e);
//		}
//		if ((oper == null) && (userLogin.equals("admin"))){
//			try {
//				
//				oper = new Oper();
//				oper.setName("Administrator");
//				oper.setLogin("admin");
//				oper.setPassphrase("");
//				oper.setVersionRoot(oper);
////				opers.setRights(this);
//				opers.setObject(oper);
//				opers.create();
//
//
//			} catch (Exception e) {
//				logger.error("",e);
//			}
//		}
//		if (oper == null) {
//			// TODO пользователь в источнике авторизации есть но профиля нет.
//			// Здесь обработка нужна!
//			throw new RuntimeException("Пользователь '"+userLogin+"' в источнике авторизации есть но профиля нет.");
//		}
//		Date currentDate = new  Date();
//		config.setOper(oper);
//		config.setLoginDate(currentDate);
//		config.setPeriodFrom(currentDate);
//		config.setPeriodTo(currentDate);
//	}
	
	
	private Opers getOpers(){
		try {
			return (Opers) new InitialContext().lookup("Opers/local");
		} catch (NamingException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Deprecated
	private Oper getAdminOper(){
		Opers opers = getOpers();
		List<Oper> list = opers.getByLogin("admin");
		Oper admin;
		if(list==null || list.isEmpty()){
			admin = createOper("admin", "Administrator");
			
		}else{
			admin= list.get(0);
		}
		return admin;
	}


	/**
	 * Этот метод скорее всего уйдет, когда появится проект JB-Admin
	 * @param login
	 * @param username
	 * @return
	 */
	private Oper createOper(String login, String username) {
		Oper oper;
		oper = new Oper();
		oper.setName(username);
		oper.setLogin(login);
		oper.setPassphrase("");
		oper.setVersionRoot(oper);
		
		oper.setSysUser(oper);
		oper.setVersionRoot(oper);
		oper.setVersion(null);
		oper.setOpenDate(new Date());
		oper.setTS(new Timestamp(System.currentTimeMillis()));
		oper = manager.merge(oper);
		return oper;
	}
	
	@Override
	public Config getConfig() {
		logger.debug("getConfig()");
		Config config = new Config();
		
		Oper oper = getLoggedUser();
		logger.debug("getConfig() oper: " + oper);
		if(oper==null){
			oper = getAdminOper();
		}
		
		config.setOper(oper);

		return config;
	}
//	
//	public SessionprofileBean() {
//		super();
//		config = new Config();
//	}


//	@Override
//	public void setRights(Sessionprofile profile) {}

//	@Override
//	public void destroy() {}
//
//	@Override
//	public void prepassivate() {}
//
//	@Override
//	public void postactivate() {}
//
//	@Override
//	public void postconstruct() {}
//
//	@Override
//	public void predestroy() {}
}
