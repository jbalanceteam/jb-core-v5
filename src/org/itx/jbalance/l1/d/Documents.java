//package org.itx.jbalance.l1.d;
//
//import java.util.List;
//
//import javax.ejb.Local;
//
//import org.itx.jbalance.l0.h.Hdocument;
//import org.itx.jbalance.l0.s.Sdocument;
//import org.itx.jbalance.l1.common.CommonDocument;
//
//@Local
//public interface Documents extends CommonDocument<Hdocument,Sdocument> {
//	public List<Hdocument> getByOwnersByPeriod();
//	public Hdocument getObject();
//}
