package org.itx.jbalance.l1.common;

import javax.ejb.ApplicationException;

/**
 *  
 *
 */
@ApplicationException(rollback=true)
public class TransException extends Exception {

  /**
	 * 
	 */
	private static final long serialVersionUID = 568297722897517112L;

public TransException () { }

}