package org.itx.jbalance.l1.common;

import java.util.List;

import org.itx.jbalance.l0.h.Hdocument;
import org.itx.jbalance.l0.s.Sdocument;

public interface CommonDocument <D extends Hdocument<?>,S extends Sdocument>  extends CommonObject<D> {
	public List<S> getSpecificationLines(D doc);
	public S createSpecificationLine(S spec, D doc);
	public S updateSpecificationLine(S specificationLine);
	public S deleteSpecificationLine(S specificationLine);
}
