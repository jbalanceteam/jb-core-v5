package org.itx.jbalance.l1.common;

import java.util.List;

import org.itx.jbalance.l0.Ubiq;

public interface  CommonObject <D extends Ubiq<?>> extends Common<D> {
	
	public D create(D object);

	public D delete(D object);

	public D update(D object) ;
	
	public List<? extends D> getAll(Class<D> clazz);
	
	public D getByUId(Long UId, Class<D> clazz);
	
//	
//	public void search();

	public Long getCount(Class<D> clazz);
	
//	public List<? extends Ubiq> getRange(Long firstRow,Long maxRow);
}
