package org.itx.jbalance.l1.common;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l1.session.Sessionprofile;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
import org.jboss.logging.Logger;

@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 900, removalTimeoutSeconds = 1000)
public class CommonBean  <T extends Ubiq<?>> implements Common <T> {

	private static final long serialVersionUID = 5439577352372150207L;

	
	@EJB(name="Sessionprofile/local")
	protected Sessionprofile sessionprofile;
	
	@PersistenceContext(unitName = "JBv4_2")
	protected EntityManager manager;



	protected Ubiq create_(Ubiq ubiq) {
		ubiq.setUId(null);
		ubiq.setSysUser(sessionprofile.getConfig().getOper());
//		ubiq.setClassName(ubiq.getClass().getName());
		ubiq.setVersionRoot(ubiq);
		ubiq.setVersion(null);
		if(ubiq.getOpenDate() != null)
			throw new RuntimeException(ubiq.getClass().getSimpleName() +  " OpenDate = "+ubiq.getOpenDate()+" In what case openDate can be not null there?");
//		TODO In what case openDate can be not null there?
//		O.setOpenDate(O.getOpenDate() == null ? new Date(): O.getOpenDate());
		ubiq.setOpenDate(new Date());
		ubiq.setTS(new Timestamp(System.currentTimeMillis()));
		manager.persist(ubiq);
		return  ubiq;
	}

//	/**
//	 * 1) N - new instance 
//	 * 2) find Oo 
//	 * 3) reflect Oo to N 
//	 * 4) persist O 
//	 * 5) N.version = O 
//	 * 6) find Oi (Предыдущая версия объекта) 
//	 * 7) Oi.version = N 
//	 * 8) persist N 
//	 * 9) persist Oi
//	 * 
//	 */
	protected synchronized <K extends Ubiq> K update_(K O) {
//		Ubiq N = null, Oo = null, Oi = null;
////		Oo - это предыдущее состояние объекта
//		Oo = manager.find(O.getClass(), O.getUId());
//		
//		/**
//		 * #358: В CommonBean поправить метод update_ чтобы он не делал insert если запись не менялась 
//		 */
//		if(!Utils.isThereChanges(O, Oo)){
//			return O;
//		}
//		
//		try {
//			N = O.getClass().newInstance(); // 1
//		} catch (InstantiationException e) {
//			getLog().error("",e);
//		} catch (IllegalAccessException e) {
//			getLog().error("",e);
//		}
//		
//		Utils.copy(Oo, N);
//		N.setVersion(null);
//		N.setUId(null);
//
//		if (!(this instanceof Sessionprofile)) O.setSysUser(sessionprofile.getConfig().getOper());
//		try {
////			Oi = (Ubiq) manager.createNamedQuery("Ubiq.PrevVersion")
////					.setParameter("version", O).getSingleResult();
//			
//			Oi=getPrevVersion(O);
//		} catch (NoResultException e) {
//			Oi = null;
//		}
//
//		manager.persist(N);
//		
//		if (Oi != null) {
//			Oi.setVersion(N);
//			Oi = manager.merge(Oi);
//		}
//		
//		manager.flush();
//		N.setVersion(O);
//		
//		getLog().debug("O.version " +  (O.getVersion()==null? null:O.getVersion().getUId()));
//		getLog().debug("Oo.version " + (Oo.getVersion()==null?null:Oo.getVersion().getUId()));
//		getLog().debug("Oi.version " + (Oi.getVersion()==null?null:Oi.getVersion().getUId()));
//		getLog().debug("N.version " +  (N.getVersion()==null? null:N.getVersion().getUId()));
//		
		O = manager.merge(O);
//		O = (K) manager.find(O.getClass(), O.getUId());
		return O;
	}

	protected  <K extends Ubiq> K delete_(K O) {
//		K N = null, Oo = null, Oi = null;
//		try {
//			N = (K) O.getClass().newInstance(); // 1
//		} catch (InstantiationException e) {
//			getLog().error("",e);
//		} catch (IllegalAccessException e) {
//			getLog().error("",e);
//		}
//
//		Oo = (K) manager.find(O.getClass(), O.getUId());
//		Utils.copy(Oo, N);
//		N.setVersion(O);
//
//		O.setSysUser(sessionprofile.getConfig().getOper());
		O.setCloseDate(new Date(System.currentTimeMillis()));
//		try {
////			Oi = (Ubiq) manager.createNamedQuery("Ubiq.PrevVersion")
////					.setParameter("version", O).getSingleResult();
//			
//			Oi = getPrevVersion(O);
//			
//		} catch (NoResultException e) {
//			Oi = null;
//		}
//
//		if (Oi != null) {
//			Oi.setVersion(N);
//			manager.persist(Oi);
//		}
		O = manager.merge(O);
//		N.setUId(null);
//		manager.persist(N);
		return O;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected <K extends Ubiq> K getPrevVersion(K O) {
		return (K) manager.createQuery("from "+O.getClass().getSimpleName()+" o where o.version = :version")
		.setParameter("version", O).getSingleResult();
	}
	
	/**
	 * Возвращает историю изменений записи. 
	 * История отсортирована по убыванию. Т.е. 1-й объект в списке будет актуальный.
	 * Последний объект в списке - первая версия.
	 */
	@SuppressWarnings("unchecked")
	public List<T> getHistory(T O) {
		return (List<T>) manager.createQuery("from "+O.getClass().getSimpleName()+" o where o.versionRoot = :versionRoot order by UId desc")
		.setParameter("versionRoot", O).getResultList();
	}
	

	protected Ubiq<?> getByUId(Ubiq<?> O) {
		return manager.find(O.getClass(), O.getUId());
	}

	public EntityManager getManager() {
		return manager;
	}
	
	public Logger getLog(){
		return Logger.getLogger(getClass());
	}
	
}
