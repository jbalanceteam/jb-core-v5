package org.itx.jbalance.l1.common;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.itx.jbalance.l0.Ubiq;

public class CommonObjectBean<D extends Ubiq<?>> extends CommonBean<D> implements CommonObject<D> {
	protected final static int MAX_SEARCH_STRING_SIZE = 10;

	private static final long serialVersionUID = -3166304145281052141L;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public D create(D object) {
		return (D) create_(object);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public  D delete(D object) {
		return delete_(object);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public D update(D object) {
		final D old = (D) manager.find(object.getClass(), object.getUId());
		getLog().info(old.getUId() + "\n" 
				+ "old nanos: "+ old.getTS().getNanos()+ "\n" 
				+ "new nanos: "+object.getTS().getNanos()+"\n\n" 
				+ "old time: "+ old.getTS().getTime()+ "\n" 
				+ "new time: "+object.getTS().getTime()+"\n" 
				+ object.getTS().equals(old.getTS()));
//		return update_(old);
		return update_(object);
	}

	@Override
	public List<? extends D> getAll(Class clazz) {
		return manager.createNamedQuery(clazz.getSimpleName()+ ".All").getResultList();
	}
//
//	public List<? extends Ubiq> getRange(Long firstRow, Long maxRow) {
//		getLog().debug("getRange("+firstRow+","+maxRow+")");
//		List tmp = constructQuery().setFirstResult(firstRow.intValue())
//				.setMaxResults(maxRow.intValue()).getResultList();
//		return tmp;
//	}

	@Override
	public Long getCount(Class<D> clazz) {
		if(clazz==null)
			throw new RuntimeException();
			return ((Number) manager.createNamedQuery(
					clazz.getSimpleName() + ".AllCount")
					.getSingleResult()).longValue();
	}

	@Override
	public D getByUId(Long UId, Class<D> clazz) {
		getLog().debug("getByUId("+UId+","+clazz+")");
		if(UId==null){
			throw new RuntimeException("UId mustn't be null");
		}
		
		if(clazz==null){
			throw new RuntimeException("clazz mustn't be null");
		}
		return manager.find(clazz, UId);
	}
}
