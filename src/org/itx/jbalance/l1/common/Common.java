package org.itx.jbalance.l1.common;

import java.io.Serializable;
import java.util.List;

import org.itx.jbalance.l0.Ubiq;

public interface Common <T extends Ubiq<?>> extends Serializable {
//	public void setRights(Sessionprofile profile);
//	public EntityManager getManager();
//	void destroy();
//	void prepassivate();
//	void postactivate();
//	void postconstruct();
//	void predestroy();
	
	List<T> getHistory(T O);
	
}
