package org.itx.jbalance.l1.o;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.itx.jbalance.l1.o.Contractors;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateless(name="Contractors")
@LocalBinding(jndiBinding = "Contractors/local")
@RemoteBinding(jndiBinding = "Contractors/remote")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ContractorsBean extends CommonObjectBean<Contractor> implements Contractors, ContractorsRemote {

	private static final long serialVersionUID = 6530056866449195444L;

//	@SuppressWarnings("unchecked")
//	public List<Contractor> getRangeWSearch(Long first, Long last, Contractor contractor)
//	{
//		setObject(contractor);
//		return (List<Contractor>)getRange(first, last);
//	}
}
