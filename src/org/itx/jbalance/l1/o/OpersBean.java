package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;


@Stateless(name = "Opers")
@LocalBinding(jndiBinding = "Opers/local")
@RemoteBinding(jndiBinding = "Opers/remote")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class OpersBean extends CommonObjectBean<Oper> implements Opers,OpersRemote {

	private static final long serialVersionUID = 266450029800037219L;




	@Override
	@SuppressWarnings("unchecked")
	public List<Oper> getByLogin(String value) {
		return manager.createNamedQuery("Oper.ByLogin")
				.setParameter("Login", value).getResultList();
	}

	
	@Override
	public Oper create(Oper oper) {
		if(oper == null || oper.getPassphrase() == null)
			throw new NullPointerException();
		
		
//		TODO: нам это нужно?
//		MessageDigest md = null;
//		try {
//			md = MessageDigest.getInstance("MD5");
//		} catch (Exception e) {
//			getLog().error("",e);
//		}
//		byte[] passwordBytes = getObject().getPassphrase().getBytes();
//		byte[] hash = md.digest(passwordBytes);
//		getObject().setPassphrase(Util.encodeBase64(hash));
		return super.create(oper);
	}

//	@Override
//	public void update() throws JBException {
////		System.out.println("____________________________________"+getObject().getPassphrase());
//		if(getObject().getPassphrase().length()!=0){
//		MessageDigest md = null;
//		try {
//			md = MessageDigest.getInstance("MD5");
//		} catch (Exception e) {
//			getLog().error("",e);
//		}
//		byte[] passwordBytes = getObject().getPassphrase().getBytes();
//		byte[] hash = md.digest(passwordBytes);
//		getObject().setPassphrase(Util.encodeBase64(hash));
//		}
////		System.out.println("____________________________________"+getObject().getPassphrase());
//		super.update();
//	}

}
