package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Contractors extends CommonObject<Contractor> {

}
