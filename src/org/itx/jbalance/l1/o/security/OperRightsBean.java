//package org.itx.jbalance.l1.o.security;
//
//import javax.ejb.Stateful;
//
//import org.itx.jbalance.l0.o.security.OperRight;
//import org.itx.jbalance.l1.common.CommonObjectBean;
//import org.jboss.annotation.ejb.LocalBinding;
//import org.jboss.annotation.ejb.RemoteBinding;
//import org.jboss.annotation.ejb.cache.simple.CacheConfig;
//
//@Stateful
//@LocalBinding(jndiBinding = "OperRights/local")
//@RemoteBinding(jndiBinding = "OperRights/remote")
//@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
//public class OperRightsBean extends CommonObjectBean<OperRight> implements OperRights,OperRightsRemote {
//
//	private static final long serialVersionUID = 6530056866449195444L;
//
//	public OperRightsBean() {
//		super(new OperRight());
//	}
//
//	public OperRight getObject() {
//		return super.getObject();
//	}
//}
