//package org.itx.jbalance.l1.o.security;
//
//import javax.ejb.Stateful;
//
//import org.itx.jbalance.l0.o.security.SystemObject;
//import org.itx.jbalance.l1.common.CommonObjectBean;
//import org.jboss.annotation.ejb.LocalBinding;
//import org.jboss.annotation.ejb.RemoteBinding;
//import org.jboss.annotation.ejb.cache.simple.CacheConfig;
//
//@Stateful
//@LocalBinding(jndiBinding = "SystemObjects/local")
//@RemoteBinding(jndiBinding = "SystemObjects/remote")
//@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
//public class RolesBean extends CommonObjectBean<SystemObject> implements SystemObjects,SystemObjectsRemote {
//
//	private static final long serialVersionUID = 6530056866449195444L;
//
//	public RolesBean() {
//		super(new SystemObject());
//	}
//
//	public SystemObject getObject() {
//		return (SystemObject) super.getObject();
//	}
//}
