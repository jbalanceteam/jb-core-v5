//package org.itx.jbalance.l1.o.security;
//
//import javax.ejb.Stateful;
//
//import org.itx.jbalance.l0.o.security.SystemAction;
//import org.itx.jbalance.l1.common.CommonObjectBean;
//import org.jboss.annotation.ejb.LocalBinding;
//import org.jboss.annotation.ejb.RemoteBinding;
//import org.jboss.annotation.ejb.cache.simple.CacheConfig;
//
//@Stateful
//@LocalBinding(jndiBinding = "SystemActions/local")
//@RemoteBinding(jndiBinding = "SystemActions/remote")
//@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
//public class SystemActionsBean extends CommonObjectBean<SystemAction> implements SystemActions,SystemActionsRemote {
//
//	private static final long serialVersionUID = 6530056866449195444L;
//
//	public SystemActionsBean() {
//		super(new SystemAction());
//	}
//
//	public SystemAction getObject() {
//		return (SystemAction) super.getObject();
//	}
//}
