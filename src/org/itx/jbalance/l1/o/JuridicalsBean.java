package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Remote(JuridicalsRemote.class)
@Stateless(name = "Juridicals")
@RemoteBinding(jndiBinding = "Juridicals/remote")
@LocalBinding(jndiBinding = "Juridicals/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class JuridicalsBean extends CommonObjectBean<Juridical> implements Juridicals {

	private static final long serialVersionUID = 5177964569087956836L;

	@Override
	@SuppressWarnings("unchecked")
	public List<Juridical> getByName(String Value) {
		return manager.createNamedQuery("Juridical.ByName").setParameter("Name", Value).getResultList();
	}
	

	@Override
	@SuppressWarnings("unchecked")
	public List<Juridical> getBySameName(String value) {
		return manager.createNamedQuery("Juridical.SearchSameName").setParameter("Name", value).getResultList();
	}

	@Override
	public Juridical getJuridical(Juridical j) {
		return manager.find(Juridical.class, j.getUId());
	}
}
