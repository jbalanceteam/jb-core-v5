package org.itx.jbalance.l1.o;



import javax.ejb.Local;

import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Physicals extends CommonObject<Physical> {
	public Physical AddPhysical(Physical p);
	public Physical UpdatePhysical(Physical p);

}
