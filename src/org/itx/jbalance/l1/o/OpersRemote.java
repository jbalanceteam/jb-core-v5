package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Remote;

import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface OpersRemote extends CommonObject <Oper>{

	public List<Oper> getByLogin(String Value);

}
