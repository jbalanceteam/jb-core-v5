package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateless
@LocalBinding(jndiBinding = "Divisions/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class DivisionsBean extends CommonObjectBean implements Divisions
{
	private static final long serialVersionUID = -3174607204964617259L;


	
	@Override
	@SuppressWarnings("unchecked")
	public List<Division> getAll(){
			return manager.createNamedQuery("Division.All").getResultList();
	}
}
