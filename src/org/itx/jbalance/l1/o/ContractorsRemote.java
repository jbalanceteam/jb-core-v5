package org.itx.jbalance.l1.o;

import javax.ejb.Remote;

import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface ContractorsRemote extends CommonObject<Contractor> {

}
