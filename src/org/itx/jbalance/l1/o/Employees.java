package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Employee;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Employees extends CommonObject<Employee> {

	
}
