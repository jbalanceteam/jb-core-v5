package org.itx.jbalance.l1.o;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.Employee;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateless(name="Employees")
@LocalBinding(jndiBinding = "Employees/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class EmployeesBean extends CommonObjectBean<Employee> implements Employees {

	private static final long serialVersionUID = 266450029800037219L;
}
